# Add Progression for `1-SoldierPack`

This series of modlets seeks to re-balance entites spawned into NPC POIs,
as well as entities spawned into biomes.

In the original NPC pack, all NPCs spawn into sleeper volumes at equal probabilities.
A player has an equal probability of encountering the same NPC at gamestage 400
as they did at gamestage 1.
And at all gamestages, they have an equal probability of encountering a "tough" NPC
(say, one with an M60) as they do of encountering an "easy" NPC (say, one with a club).

The same is true for entities spawned into biomes.
However, biome spawns are not themselves affected by gamestage.
Instead, we must determine difficulty in other ways:

* The difficulty of the biome itself (similar to loot stage).
* Whether the entities are spawning in downtown areas.
* Whether the entities are spawning during the day or at night.

## Features

These are the features I prioritized:

1. **Character difficulty should be determined by gamestage or biome difficulty.**  
    Players should be more likely to encounter "easy" NPCs at lower gamestages or easier biomes,
    and "tough" NPCs at higher gamestages or harder biomes.
    This applies to both the characters themselves (health and damage resistance),
    and the weapons they wield (club vs. rocket launcher).
2. **Characters will many weapon varieties should _not_ overwhelm those with few weapon varieties.**  
    If character A can wield 20 weapons, and character B can wield 5 weapons,
    it should _not_ be four times as likely for players to encounter character A.
    There should be a (roughly) equal chance to encounter either character.
3. **Groups that spawn characters with all weapons, should be consistent with**
    **groups that spawn characters with only melee or ranged weapons.**  
    For example, a machete is a "tough" _melee_ weapon,
    but nearly all _ranged_ weapons are "tougher" than it.
    A machete-wielding NPC should **not** be too "tough" to spawn in a gamestage 1 _melee_ group,
    but "weak" enough to spawn with a high probability in a gamestage 1 _"all"_ group.

## Technical Details

This modlet uses XPath to modify XML files.

However, it depends upon the `0-XNPCCore` modlet.
That modlet, in turn, depends upon `0-SCore`.
Those modlets include new C# code and assets,
so they must be installed on both clients and servers,
and EAC **must** be turned off.

This modlet only affects the probabilities that existing entities will be spawned.
It does not introduce new items, recepes, characters, etc.
For that reason, it _might not_ be necessary to start a new game after installation.

However, I still suggest starting a new game, just to be sure.

### How probabilities are calculated for NPCs

These details can be found in the
[README.md file](../2-XNPCCore_khzmusik_Progression/README.md#how-probabilities-are-calculated-for-npcs)
for the `2-XNPCCore_khzmusik_Progression` modlet.
