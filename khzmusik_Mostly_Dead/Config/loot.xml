<?xml version="1.0" encoding="UTF-8"?>
<configs>
    <!--
        NOTE ABOUT LOOT TEMPLATES:

        The loot templates used for LootItem rewards are different from the ones for containers.
        The templates for containers have a "level" value that represents the loot gamestage.
        The ones for LootItem rewards have a "level" value that represents the tier.

        In general, the LootItem "value" is the QUEST TIER level (1 - 5, 6 isn't currently used).
        That level is translated to a quality level (1 - 7) by the PROBABILITY template.
        Finally, the quality level is translated to an item level (2 - 6) by the QUALITY template.
        Items that don't have a level (like medicine or ammo) use only the probability template.
        Also, bundles (like ammo/weapon bundles) set their quality levels themselves.
        
        These are the loot PROBABILITY templates to use:

        * QuestT1Prob
        * QuestT2Prob
        * QuestT3Prob

        These are also OK to use, since they have the same value regardless of level:

        * veryLow
        * low
        * medLow
        * med
        * medHigh
        * high
        * guaranteed

        These are the loot QUALITY templates to use:

        * questT1QualTemplate
        * questT2QualTemplate
        * questT3QualTemplate

        Vanilla quest loot groups (taken from the vanilla quests.xml documentation):

		* groupQuestAmmo (*)          random but higher ammo quantity than in general loot
		* groupQuestArmor             all armor excluding cloth/scrap
		* groupQuestMedical (*)       higher quality/quantity medical items and drugs
		* groupQuestWeapons           all weapons
		* groupQuestSchematics (*)    schematics like the sham secret recipe
		* groupQuestTools             tools, not stone
		* groupQuestMods (*)          all available mods
		 
        (*) not scaled for item quality but can scale for probability, like more drugs at higher gamestage

        See the vanilla quests.xml file for details.
    -->
    <append xpath="/lootcontainers">
        <!--
            Lootgroups for "StartNewCharacter" quests
        -->
        <lootgroup name="groupApparelShirts">
            <item group="groupApparelShirtLong" prob="0.5" />
            <item group="groupApparelTankTopTShirt" prob="0.5" />
        </lootgroup>
        <lootgroup name="bachelorFood">
            <item name="foodCharredMeat" count="5,10" />
            <item name="foodGrilledMeat" count="5,10" />
            <item name="foodBoiledMeat" count="5,10" />
            <item name="foodBakedPotato" count="5,10" />
            <item name="foodEggBoiled" count="10" />
            <item name="foodBaconAndEggs" count="5,10" />
            <item name="foodCornOnTheCob" count="10" />
        </lootgroup>
        <lootgroup name="grandmaFood">
            <item name="foodSteakAndPotato" count="5,10" />
            <item name="foodBlueberryPie" count="5,10" />
            <item name="foodMeatStew" count="5" />
            <item name="foodVegetableStew" count="5" />
            <item name="foodPumpkinPie" count="5" />
            <item name="foodPumpkinCheesecake" count="5" />
            <item name="foodPumpkinBread" count="5,10" />
        </lootgroup>
        <lootgroup name="cookFood">
            <item name="foodShamChowder" count="5,10" />
            <item name="foodHoboStew" count="5,10" />
            <item name="foodFishTacos" count="5,10" />
            <item name="foodChiliDog" count="5,10" />
            <item name="foodGumboStew" count="5,10" />
            <item name="foodShepardsPie" count="5,10" />
            <item name="foodSpaghetti" count="5,10" />
            <item name="foodTunaFishGravyToast" count="5,10" />
        </lootgroup>
        <!-- No longer used in version 3.0.0 (see below) -->
        <lootgroup name="rewardBeverages">
            <item name="drinkJarPureMineralWater" count="5,10" prob="0.25" />
            <item name="drinkJarYuccaJuice" count="5,10" />
            <item name="drinkJarGoldenRodTea" count="5,10" />
            <item name="drinkJarRedTea" count="5,10" />
            <item name="drinkYuccaJuiceSmoothie" count="5,10" prob="0.25" />
            <item name="drinkJarCoffee" count="5,10" />
            <item name="drinkJarBeer" count="5,10" />
        </lootgroup>
        <lootgroup name="groupStartingAmmo">
            <item name="ammo9mmBulletBall" count="200" />
            <item name="ammo44MagnumBulletBall" count="150" />
            <item name="ammoShotgunShell" count="120" />
            <item name="ammo762mmBulletBall" count="120" />
        </lootgroup>
        <lootgroup name="groupUpperArmorT0" loot_quality_template="questT1QualTemplate">
            <item name="armorClothJacket" />
            <item name="armorClothHat" />
            <item name="armorScrapHelmet" />
            <item name="armorScrapChest" />
        </lootgroup>
        <lootgroup name="groupLowerArmorT0" loot_quality_template="questT1QualTemplate">
            <item name="armorClothPants" />
            <item name="armorClothBoots" />
            <item name="armorClothGloves" />
            <item name="armorScrapGloves" />
            <item name="armorScrapLegs" />
            <item name="armorScrapBoots" />
        </lootgroup>
        <lootgroup name="groupUpperArmorT1" loot_quality_template="questT1QualTemplate">
            <item name="armorLeatherChest" />
            <item name="armorLeatherHood" />
            <item name="armorIronHelmet" />
            <item name="armorIronChest" />
        </lootgroup>
        <lootgroup name="groupLowerArmorT1" loot_quality_template="questT2QualTemplate">
            <item name="armorLeatherBoots" />
            <item name="armorLeatherPants" />
            <item name="armorLeatherGloves" />
            <item name="armorIronGloves" />
            <item name="armorIronBoots" />
            <item name="armorIronLegs" />
        </lootgroup>
        <lootgroup name="groupUpperArmorT2" loot_quality_template="questT3QualTemplate">
            <item name="armorMilitaryHelmet" mods="modArmorHelmetLight" mod_chance=".2" />
            <item name="armorMilitaryVest" />
            <item name="armorSteelChest" />
            <item name="armorSteelHelmet" mods="modArmorHelmetLight" mod_chance=".2" />
            <item name="armorSwatHelmet" mods="modArmorHelmetLight" mod_chance=".2" prob=".4" />
        </lootgroup>
        <lootgroup name="groupLowerArmorT2" loot_quality_template="questT3QualTemplate">
            <item name="armorMilitaryGloves" />
            <item name="armorMilitaryBoots" />
            <item name="armorMilitaryLegs" />
            <item name="armorSteelBoots" />
            <item name="armorSteelLegs" />
            <item name="armorSteelGloves" />
        </lootgroup>
        <!-- New for version 3.0.0 -->
        <lootgroup name="groupQuestRewardBeveragesT1">
            <item name="drinkJarBoiledWater" count="5,10" loot_prob_template="high" />
            <item name="drinkJarYuccaJuice" count="5,10" loot_prob_template="medLow" />
        </lootgroup>
        <lootgroup name="groupQuestRewardBeveragesT2">
            <item name="drinkJarGoldenRodTea" count="5,10" loot_prob_template="high" />
            <item name="drinkJarRedTea" count="5,10" loot_prob_template="high" />
            <item name="drinkJarCoffee" count="5,10" loot_prob_template="medLow" />
            <item name="drinkJarBeer" count="5,10" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardBeveragesT3">
            <item name="drinkJarPureMineralWater" count="5,10" loot_prob_template="high" />
            <item name="drinkYuccaJuiceSmoothie" count="5,10" loot_prob_template="med" />
            <item name="drinkCanMegaCrush" count="5,10" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardArmorBundlesT1">
            <item group="groupQuestT0LightArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT0HeavyArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT0LightArmorBonus" loot_prob_template="low" />
            <item group="groupQuestT0HeavyArmorBonus" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardArmorBundlesT2">
            <item group="groupQuestT1LightArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT1HeavyArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT1LightArmorBonus" loot_prob_template="low" />
            <item group="groupQuestT1HeavyArmorBonus" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardArmorBundlesT3">
            <item group="groupQuestT2LightArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT2HeavyArmor" loot_prob_template="medHigh" />
            <item group="groupQuestT2LightArmorBonus" loot_prob_template="low" />
            <item group="groupQuestT2HeavyArmorBonus" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardKnivesT1" loot_quality_template="questT1QualTemplate">
            <item name="meleeWpnBladeT0BoneKnife" />
        </lootgroup>
        <lootgroup name="groupQuestRewardKnivesT2" loot_quality_template="questT2QualTemplate">
            <item name="meleeWpnBladeT1HuntingKnife" />
        </lootgroup>
        <lootgroup name="groupQuestRewardKnivesT3" loot_quality_template="questT3QualTemplate">
            <item name="meleeWpnBladeT3Machete" />
        </lootgroup>
        <lootgroup name="groupQuestRewardFirearmBundlesT1">
            <item group="groupQuestT0Firearms" loot_prob_template="medHigh" />
            <item group="groupQuestT1Firearms" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardFirearmBundlesT2">
            <item group="groupQuestT2Firearms" loot_prob_template="medHigh" />
            <item group="groupQuestT3Firearms" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardFirearmBundlesT3">
            <item group="groupQuestT3Firearms" loot_prob_template="medHigh" />
            <item group="groupQuestT3LegendaryFirearms" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardBowBundlesT1">
            <item name="questRewardBowT0PrimitiveBowBundle" loot_prob_template="medHigh" />
            <item name="questRewardBowT1WoodenBowBundle" loot_prob_template="low" />
            <item name="questRewardBowT1IronCrossbowBundle" loot_prob_template="low" />
        </lootgroup>
        <lootgroup name="groupQuestRewardBowBundlesT2">
            <item name="questRewardBowT1WoodenBowBundle" />
            <item name="questRewardBowT1IronCrossbowBundle" />
        </lootgroup>
        <lootgroup name="groupQuestRewardBowBundlesT3">
            <item name="questRewardBowT3CompoundBowBundle" />
            <item name="questRewardBowT3CompoundCrossbowBundle" />
        </lootgroup>
    </append>
</configs>
