# Bandits Prefabs

This modlet contains new POIs that spawn Bandit NPCs in their sleeper volumes.

The new POIs started out as vanilla POIs, but most are significantly modified and re-designed,
especially the higher gamestage POIs.

## Features

This modlet contains these Bandit POIs:

### Bandit Ruins

* Tier 1
* Quests: clear, fetch

![Bandit Ruins](./Prefabs/POIs/bandits_site.jpg)

### Bandit Camp

* Tier 2
* Quests: clear, fetch

![Bandit Camp](./Prefabs/POIs/bandits_site_2.jpg)

### Bandit Settlement

* Tier 3
* Quests: clear, fetch

![Bandit Settlement](./Prefabs/POIs/bandits_settlement.jpg)

### Bandit Oil Well

* Tier 4
* Quests: clear, fetch

![Bandit Settlement](./Prefabs/POIs/bandits_oil_well.jpg)

### Bandit Theater
_aka "Panem et Circenses"_

* Tier 5
* Quests: clear, fetch

![Bandit Theater](./Prefabs/POIs/bandits_panem_et_circenses.jpg)

### Bandit Refinery

* Tier 5
* Quests: clear, fetch

![Bandit Refinery](./Prefabs/POIs/bandits_refinery.jpg)

## Dependent mods and modlets

These POIs are designed to be used with NPC Core and Bandit NPCs.

However, they _can_ be used with zombies, but only if you don't want NPCs in your game at all.

### Using with Bandit NPCs

Human NPCs are dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

* SCore:
  * [Git repo](https://github.com/SphereII/SphereII.Mods/tree/master/0-SCore)
  * [.zip file](https://gitlab.com/sphereii/SphereII-Mods/-/archive/master/SphereII-Mods-master.zip?path=0-SCore)
    (copy _only_ the `0-SCore` subfolder into your Mods folder)
* NPC Core:
  * [Forum link](https://community.7daystodie.com/topic/26974-npcmod-a-community-project/)
  * [.zip file](https://github.com/7D2D/A20Mods/blob/main/0-XNPCCore.zip)

While it will work with only those two modlets, the only Bandit NPC that comes with NPC Core is Harley.
You will probably want to install NPC Pack modlets that add additional bandits to the game.

I recommend these modlets:

* Rogues and Psychos Pack
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/1-khzmusik_NPC_Rogues_and_Psychos)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=1-khzmusik_NPC_Rogues_and_Psychos)
    (copy _only_ the `1-khzmusik_NPC_Rogues_and_Psychos` subfolder into your Mods folder)
* RaiderGurlz Pack
  * [Forum link](https://community.7daystodie.com/topic/26974-npcmod-a-community-project/)
  * [Git repo](https://github.com/7D2D/A20Mods/tree/main/1-RaiderGurlzPack)
  * [.7z file](https://github.com/7D2D/A20Mods/blob/main/1-RaiderGurlzPack.7z)
* Raiderz Pack
  * [Forum link](https://community.7daystodie.com/topic/26974-npcmod-a-community-project/)
  * [Git repo](https://github.com/7D2D/A20Mods/tree/main/1-RaiderzPack)
  * [.7z file](https://github.com/7D2D/A20Mods/blob/main/1-RaiderzPack.7z)

These modlets have custom C# code and/or assets.
They must be installed on both clients and servers.
You must turn EAC off when launching the game.

To adjust the probabilities of the Raiderz and RaiderGurlz,
so that they are properly gamestaged when spawned into POIs,
I additionally recommend installing these progression modlets:

* Progression modlet for RaiderGurlz Pack
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/1-RaiderGurlzPack_khzmusik_Progression)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=1-RaiderGurlzPack_khzmusik_Progression)
    (copy _only_ the `1-RaiderGurlzPack_khzmusik_Progression` subfolder into your Mods folder)

* Progression modlet for Raiderz Pack
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/1-RaiderzPack_khzmusik_Progression)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=1-RaiderzPack_khzmusik_Progression)
    (copy _only_ the `1-RaiderzPack_khzmusik_Progression` subfolder into your Mods folder)

By default, NPCs in sleeper volumes spawn in "always awake."
I find it better if they obey the rules of the sleeper volumes.
If this sounds like something you want, then I recommend also installing this modlet:

* Variable NPC Sleepers
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/2-khzmusik_variable_NPC_sleepers)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=2-khzmusik_variable_NPC_sleepers)
    (copy _only_ the `2-khzmusik_variable_NPC_sleepers` subfolder into your Mods folder)

### Using with zombies

If you want to use these prefabs,
but don't want to install _any_ NPCs into the game _at all,_
then you can install this modlet:

* NPC Prefabs Spawn Zombies
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/0-XNPCCore_khzmusik_Prefabs_Spawn_Zombies)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=0-XNPCCore_khzmusik_Prefabs_Spawn_Zombies)
    (copy _only_ the `0-XNPCCore_khzmusik_Prefabs_Spawn_Zombies` subfolder into your Mods folder)

> :warning: This modlet is **incompatible with** SCore and NPC Core.
> If you want to use those modlets, or if you use an overhaul or other mod that uses those modlets,
> you *can not* install this modlet.

That modlet uses XPath to modify XML files, and does not use custom C# code.
It should be compatible with EAC.
It does _not_ contain any new assets (such as images or models).
Servers should automatically push the XML modifications to their clients, so separate client
installation should _not_ be necessary.

If you _do_ want NPCs in the game, but want _these particular_ POIs to spawn zombies,
then you need to modify the POIs using the prefab editor.
See the technical details section below.

### Localizing the POI names

This modlet includes localized strings for the POI names.
However, by default, the game does not support translating the names of prefabs.

Some overhaul mods (e.g. Undead Legacy) already have support for POI translations.
If you are using one of those overhauls, you probably do not need to do anything.

Otherwise, you can use this modlet:

* Localize Prefabs
  * [Forum link](https://community.7daystodie.com/topic/27333-a20-khzmusiks-modlets/)
  * [Git repo](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/tree/main/khzmusik_Localize_Prefabs)
  * [.zip file](https://gitlab.com/karlgiesing/7d2d-a20-modlets/-/archive/main/7d2d-a20-modlets-main.zip?path=khzmusik_Localize_Prefabs)
    (copy _only_ the `khzmusik_Localize_Prefabs` subfolder into your Mods folder)

That modlet includes custom C# code.
It must be installed on both clients and servers.
You must turn EAC off when launching the game.

## Technical Details

This modlet should be installed into the `Mods` folder,
the same as any other modlet.

Like all modlets that contain POIs,
it must be installed on both clients and servers.

You should create a new RWG map after installing this modlet.

### Creating the POIs

These POIs were created with the 7D2D prefab editor,
using the custom NPC sleeper volumes provided by NPC Core.

They may also use a custom block provided by SCore,
which allows POI designers to issue "orders" to the NPCs around that block.

Other than that, there is no technical difference between these POIs and "vanilla" POIs.

### Editing the POIs to use zombies

If you want to have NPCs in the game,
but want _particular_ POIs to spawn zombies instead of NPCs,
then you will need to edit the POIs in the 7D2D prefab editor.

This involves using zombie spawn groups rather, than NPCs spawn groups, in the sleeper volumes.

To do this:

1. From the game's loading screen, select "Editing Tools".
1. Select "Level/Prefab Editor".
1. Once the prefab editor is open, hit "esc" to bring up the editing menus.
1. On the top right, you will see the level tools window, with icons representing four tabs.
  Select the fourth tab (with the "house" icon).
  This opens the prefab browser.
1. In the second search box, type in the name of the POI that you want to edit.
  You can also search for a partial name, like "bandit", and select from the results.
1. Click the "Load" button at the bottom to load the POI.
2. In the level tools window, click the second tab (with the "hammer and brick" icon).
  This opens the Level Tools window.
1. Check "Show Sleeper Volumes".
  The blue borders of the sleeper volumes should now show.
  (If they do not, try saving the POI - that usually fixes the issue.)
1. Click "esc" to leave the editing menus.
2. Select each sleeper volume by targeting it with your mouse and left-clicking it.
  You might need to have a block in your hand.
  Sometimes sleeper volumes can be tricky to select, just keep trying.
1. With a sleeper volume selected, hit "k" to edit the sleeper volume.
  An editing window should open.
1. In the search bar in the editing window, search for a different sleeper volume spawn group.
  If in doubt, a useful group is "Group Generic Zombie".
  For tougher POIs, you could use "Group Zom Badass" or "Group Zom Badass Only".
1. Click on the spawn group name.
  In the upper-right hand window, the "Group:" should show the spawn group you selected.
1. Repeat these steps for _all_ the sleeper volumes in the POI.
2. Save the POI.

This should be sufficient.
If you load the POI, it should now spawn zombies rather than human NPCs.

If there are (invisible) SCore blocks in the prefab, then they will still be there;
but this should not make any difference, since zombies ignore them.

The POIs might need to be rebalanced, since human NPCs are usually "tougher" than zombies.
For example, a tier 3 NPC POI might be a tier 2 if spawning zombies.
As such, the tier level may need to be adjusted in the prefab XML,
or the loot might need to be nerfed.

But that is a matter of opinion, and you do not need to rebalance anything if you don't want to.

## License

Any rights that I hold in these POIs, I dedicate to the
[public domain](https://creativecommons.org/publicdomain/zero/1.0/).

You may use them in mods or overhauls, edited or verbatim, without my permission.
Credit is appreciated but not required.

I do not claim any rights over The Fun Pimps assets,
nor over any of the custom assets or code from SCore or NPC Core.
You must still abide by their licenses.
