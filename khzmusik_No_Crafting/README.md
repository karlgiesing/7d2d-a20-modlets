# No Crafting

Removes all crafting from the game.
This includes cooking, so farming is also removed.

This mod was inspired by
[the Lucky Looter series](https://youtube.com/playlist?list=PLc594J9-WhOxPuElG9lPf8W9Emwr0P2uW)
from [Glock9](https://www.youtube.com/@Glock9).

## Features

* The only items the player can craft are the bedroll and land claim.
* Removed all Basics of Survival quests except for crafting a bedroll.
  (After completion, players still get the skill points and Whiteriver Citizen quest.)
* Most player-crafted workstations are removed from the game (destroyed versions are still in game).
  The exception is campfires, which cannot be crafted, but can be used for warmth if found in the wild.
* Reworked perks:
  * Crafting recipes and bonuses are removed from all perks.
  * Master Chef, Living Off The Land, and Grease Monkey are removed completely.
* Schematics, workstations, recipes, and parts for assembled weapons, tools, and vehicles are removed from
  loot containers, trader stashes, quest rewards, and Twitch actions.
* Removed perk books that only give crafting recipes (e.g. Needle and Thread books);
  completion bonuses that only give recipes (e.g. stacks of ammo)
  now give experience buffs instead.
* Items or mods which could only be crafted (e.g. Fireman's Helmet) are added to
  loot containers and trader stashes.
* Crafting-related loading screen tips are removed.
* Localizations are updated to remove any mention of crafting, cooking, or forging
  (where possible, I'm sure I missed a lot).

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.
It does not contain any additional images or Unity assets.
It is safe to install only on the server.

However, this mod is **not** save-game safe.
_You must start a new game after installing or removing this mod._
