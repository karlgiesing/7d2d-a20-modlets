# NPC Core: Enemies Fight Horde NPCs

NPC Core has a variety of entity class templates that NPC Pack authors can use for their characters.
Some of those templates allow the NPCs to be spawned into hordes (wandering hordes or blood moon hordes).
Those characters use a specific C# class that is different from the other NPC templates.

Unfortunately, by default, vanilla enemies (zombies, animals, etc.) will not fight those characters.
They won't even defend themselves if those characters attack first.

> :information_source: Many NPC Pack authors do not use these templates at all.
> Even their "simple" NPCs use templates for characters which cannot be spawned into hordes.
> If you are only using those characters, you do not need this mod.

## Features

If one of those NPC Core characters attacks a vanilla enemy, the vanilla enemy will fight back,
damaging (and possibly killing) the character that attacked it.

Vanilla enemies will still not _initially target_ those NPC Core characters.
This is a limitation of the vanilla game.

See the Technical Details section if you want a technical explanation.

## Dependent and Compatible Modlets

This modlet is dependent upon these other modlets.

* `0-SCore`: for C# code and features needed by NPC Core
* `0-XNPCCore`: for the templates of NPCs that can be spawned into hordes

Not all NPC Packs provide characters that can spawn into hordes.
These are the only packs that I know of which do:

* `1-khzmusik_NPC_Rogues_and_Psychos`
* `1-khzmusik_NPC_Whisperers`

## Technical Details

This modlet uses XPath to modify XML files, and does not require C#.

However, it is dependent upon `0-XNPCCore`, which has additional assets.
That modlet is dependent upon `0-SCore`, which has additional C# code.
These modlets *must* be installed on the server, and on all clients.
Additionally, EAC *must* be turned off.

In theory, it should work if you install _this_ modlet only on the server,
so long as all clients have `0-XNPCCore` and `0-SCore` installed.
But since clients already have to install mods, they might as well install this one too.

### The issue

If NPC designers want their characters to be spawned into hordes,
those characters *must* descend from the C# `EntityEnemy` class.
This is a limitation of the vanilla horde spawning code.

NPC Core provides templates for these kinds of characters.
Those templates have "BM" (for "blood moon") in their names.
The templates use a custom C# class from SCore, called `EntityEnemySDX`,
that was created specifically for this purpose.
That class descends from the C# `EntityEnemy` class, and adds some very basic NPC behaviors.

The problem is that `EntityEnemySDX` is not in any of the vanilla AI tasks or targets,
such as "ApproachAndAttackTarget".
Those are defined as C# classes in the "data" attribute, and are a comma-separated list.

Unfortunately, there is no way to add `EntityEnemySDX` to those lists,
because that class is located in SCore.dll.
When parsing classes in XML properties, the only way to tell the game where a C# class is located,
is to add a comma and the .dll name: "EntityEnemySDX, SCore".

That obviously won't work if it's in a comma-separated list.
The game will think you're referring to two different classes, `EntityEnemySDX` and `SCore`,
located in the game's own .dll file.

This means any solution must involve the base `EntityEnemy` class,
which is the class that all vanilla enemies use.

### The solution

Unfortunately, you can't add the base `EntityEnemy` class to "SetNearestEntityAsTarget".
If you do, then vanilla enemies will target _each other._
(They won't actually damage each other, but it is still an unacceptable solution.)

This means vanilla entities can't _initially target_ the horde characters.

However, we _can_ add that class to the other AI tasks:

* "SetAsTargetIfHurt": This will cause vanilla enemies to target the `EntityEnemy` _that damages it._
    (Since they can't damage each other, they will not target each other.)
* "ApproachAndAttackTarget": This will allow the vanilla enemy to _actually attack_ the `EntityEnemy`.

With these changes, vanilla enemies will defend themselves when attacked by characters.
