# Human Faction Reputation and Quests

Introduces a Reputation mechanic, which is affected by killing NPCs.

Also includes quests that can change the player's relationship with specific factions,
with buffs that can be used as quest actions that affect the player's reputation.

## Faction Relationship Tiers

This is how The Fun Pimps have set up faction relationships in 7D2D.

The relationship between a faction and another faction is represented as
a number between 0 and 1000, inclusive.
Each player has their own (unnamed) faction,
so NPC factions can have different relationships with individual players.

That range is divided into five named relationship tiers:

* hate: 0 - 199
* dislike: 200 - 399
* neutral: 400 - 599
* like: 600 - 799
* love: 800 - 1000

## New Features

### Murder affects reputation

_Players can damage and kill NPCs of any faction._

If you kill a human NPC, your relationship with that NPC's faction decreases. 
But, your relationships with other factions increases or decreases, depending upon the
relationships between those factions and the faction of the NPC you killed.

Your change in reputation depends upon the specific NPC faction.
For example, non-bandit factions don't care much if you kill bandits,
and even other bandits don't care that much.
On the other hand, killing a Whiteriver NPC will have much larger consequences.

NPCs will not initially attack if they are in a faction that does not "hate" the player.
But if the player damages them, the NPCs _will_ fight back,
regardless of the player's reputation with their faction.
They will only stop attacking the player if they are damaged by something else,
if they lose sight of the player (for 15 real seconds - I think),
or if the player kills them.

So be careful with your glancing blows, and don't be ashamed to run away!

### Reputation buffs

When applied, these buffs increase (or decrease) your reputation (relationship value) with a
faction that you help (or harm).

Some buffs also decrease (or increase) your reputation with the other factions, depending upon the
relationships between those factions and the faction you just helped (or harmed).

These buffs can be used by quest actions,
where the action is fired during the phase where the user returns to the quest giver.
The quests in `quests.xml` use these buffs, and can be used as examples to create your own quests.

The buffs display an icon and a toolbelt message when they are activated,
so players will be aware that they just changed their reputation.

### Reset reputation on death

This is an optional buff that will reset the player's relationships with all factions upon death.
It is meant to be used in "dead is dead" (or "mostly dead") play styles.

**This feature is not enabled by default.**

See the Technical Details section for instructions about enabling this feature.

### Quests affecting reputations

Traders now give special quests that will improve the player's reputation with a faction.
Quests can either go to human POIs, or not.

#### Quests that don't require human POIs

* _Prove yourself:_ prove yourself worthy by killing waves of zombies at a random location.
    This quest is given by a faction that initially dislikes or hates the player.
* _Snipe hunt:_ looks like a buried treasure quest, but is really a practical joke.
    Has terrible reward loot, but double the XP, and gives you a mocking note from the faction.
    It exists mainly to increase your standing with the faction that tricked you.

#### Quests that require human POIs

* _Defend:_ defend a human POI against waves of zombies.
    This quest is given by a faction that initially likes or is neutral to the player.
* _Steal:_ equivalent of a fetch or hidden cache quest.
    The faction giving the quest has been robbed by some other faction (or so they say),
    and you need to steal the stash back.
* _Murder:_ equivalent of a clear quest.
    The faction giving the quest has determined that the humans in another faction's POI
    do not deserve to live.

**These quests are not enabled by default.**
There are not currently enough human POIs to support them.
Hopefully that will change in the near future.

See the Technical Details section for instructions about enabling this feature,
and the POI requirements that you need to meet in order to support these quests.

## Dependent Modlets

This modlet is dependent upon these other modlets.

* `0-SCore`: for C# code and features
* `0-NPCCore`: for the human faction definitions

## Technical Details

This modlet uses XPath to modify XML files, and does not use custom C# code.

However, the dependent modlets _do_ use custom C# code, and contain custom assets.
They are _not_ compatible with EAC, and _must_ be installed on both servers and clients.

### Defining Faction Relationships

The relationship tiers are set in `npc.xml` in each faction's `<faction>` tag.

When specified by tier name, the number is the **lowest** number in the range
(so, "hate" is zero, "neutral" is 400, and so on).

In the XML, the `<relationship>` tag value of "*" means
"members of any faction not explicitly named in another `<relationship>` tag value
(other than members of the same faction)."
This is also how a faction defines its (initial) relationship to players.

A member of a faction always has a relationship of "love" with other members of its faction.
(This is hard-coded by TFP.)

Please keep in mind that _factions are unrelated to follower NPCs._
A follower NPC automatically "adopts" the faction of its leader (usually the player).
If the player had a good enough reputation with bandits to hire one,
but then kills a bunch of bandits so their reputation goes down again,
their hired bandit won't attack them, and will attack other bandits.

### The Murder Reputation mechanic

Here's how I enabled the mechanic where the players can kill NPCs,
and their faction reputations change when they do.

**1. Enable the ability for players to damage non-enemy NPCs**

With the default NPC Core settings, players cannot damage NPCs unless their relationship
tier with the NPC's faction is "Dislike" or lower.
They are prevented from damaging neutral or friendly factions.

Without enabling the ability for players to damage _non-enemy_ NPCs,
players could only decrease their faction standing with NPCs that are already enemies,
and could only increase their faction standing with NPCs that are already friendly or neutral.

To enable this ability, I added the "DamageRelationship" cvar to the player,
and set its value to a level that is higher than the default of "Neutral."

The value of 1001 requires the NPC be in a faction that has a relationship with the player that
is _higher_ than `Love`, in order for that NPC to avoid damage.
No factions have a relationship with the player that is greater than `Love`,
so this value will mean **all** NPCs can be damaged.

_Note:_ This will only allow the _player_ to damage _NPCs._
Non-enemy NPCs will still not be able to damage the player, _unless_ the player damages them first.

**2. Add the "buffMurderReputation" buff to the player**

This is the buff that actually performs the reputation changes.
It is added to the player immediately upon entering the game,
and remains enabled on the player at all times.

The easiest way to do this was to add it to the vanilla "buffStatusCheck01" buff,
and add the buff using a `triggered_effect` with the "onSelfEnteredGame" trigger.

### Enabling the Reset Reputation On Death mechanic

**Add the "buffResetFactionRelationshipsOnDeath" buff to the player**

This is the buff that resets the faction relationships when the player dies.
It should be added to the player immediately upon entering the game,
and remain enabled on the player at all times.

The easiest way to do this is to add it to the vanilla "buffStatusCheck01" buff,
and add the buff using a `triggered_effect` with the "onSelfEnteredGame" trigger.

There is commented-out code in `buffs.xml` that will do this.
Simply un-comment that code to add the buff to the player.

### Enabling the quests that require human POIs

There is commented-out code in `quests.xml` that will do this.
Simply un-comment the code, and traders will give those quests.

Be aware that you **must** have the POI prefabs to support these quests.
Otherwise, the quests will throw an error because they can't find a matching POI.

These should not be added to trader quests unless the player is guaranteed to have
at least one POI that:

* is within range of the trader (2000 blocks/meters if the range isn't specified);
* accepts fetch or clear quests (in its "QuestTags" property) for "steal" or "murder" quests;
* has the proper human faction tag (in its "Tags" property);
* is in the matching tier (in its "DifficultyTier" property) - quests use T2 to T5; and
* spawns the proper NPCs (uses the proper sleeper volume groups from NPCCore).

In practical terms this means you must have at least one POI per human faction per tiers 2 - 5
(and ideally more than one).
That is a _minimum_ of 24 POIs.

Of course, you can also enable only those quests for which you have POIs.
It is probably better to have POIs for multiple factions at the same level
(so, it is better to have all factions but only T5 POIs, than T2 - T5 POIs but only one faction).
Otherwise the player will be forced to side against only those factions that have POIs,
which makes the reputation system very one-sided.
