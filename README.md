# 7D2D A20 Modlets

Modlets (small modifications) for the game 7 Days To Die (aka 7D2D):
[7daystodie.com](https://7daystodie.com/)

I am user [khzmusik](https://community.7daystodie.com/profile/51265-khzmusik/) on the 7D2D forums.

By convention, modlet authors create new repos for each alpha version of 7D2D.
These modlets were created for, and tested with, Alpha 20.

Repos for previous versions are here:

* [Alpha 18 modlets](https://gitlab.com/karlgiesing/7d2d-modlets)
* [Alpha 19 modlets](https://gitlab.com/karlgiesing/7d2d-a19-modlets)

## Installation

Each modlet has an individual `README.md` file.
Under the technical details secion, it should tell you whether you need to start a new game
or generate a new world.
But if you want to be safe, generate a new game world after installing *any* modlets.

### Types of modlets

For the most part, there are three different kinds of modlets:

1. Modlets that use XPath to modify the game's XML configuration files.
    These modlets can be used with EAC.
    In multi-player games, servers will push the XML changes to the client,
    so clients do not need to install the modlet before connecting.
1. Modlets that include custom assets (Unity packages, custom icons, etc.)
    usually in addition to using XPath.
    These modlets can be used with EAC.
    However, in multi-player games, servers will **not** push the assets to the client,
    so clients **do** need to install the modlet before connecting.
1. Modlets that include custom C# code in a compiled .dll,
    usually in addition to using XPath and/or including custom assets.
    These modlets can **not** be used with EAC, so the game must be launched with EAC turned off.
    In multi-player games, servers will **not** push the .dll to the client,
    so clients **do** need to install the modlet before connecting.

> In Alpha 20, The Fun Pimps added the ability to natively load custom .dll files.
> However, not every mod with custom C# code will be able to compile it into a separate .dll file.
> Some may require changes to the game's core `assembly-csharp.dll` file.
> Those modlets will require the DMT tool and/or the Mod Launcher to install.
> This should not apply to any of my modlets, but it is a possibility with other modlets or
> "overhaul" mods.

Each individual modlet should have its own README.md file,
which will explain whether the modlet uses XPath, has custom assets, and/or has custom code.

In addition, many modlets are dependent upon other modlets being installed.
You will need to pay attention to the modlet dependencies as well.
Details about the modlet's dependencies should also be in the README.md file.

### Installation using the Mod Launcher

Using the Mod Launcher is the recommended way to install these modlets.
In addition to being easier to work with,
it can automatically update any modlets it installs,
and will transparently build any modlets that have un-compiled C# code.

The Mod Launcher can be downloaded here:

* [7d2dmodlauncher.org](http://7d2dmodlauncher.org)

Since most of these modlets are included in the Mod Launcher database,
you can follow these instructions:

* [Managing Modlets](http://7d2dmodlauncher.org/ManagingModlets.html)

I recommend creating a new My Mods, and installing modlets into that;
this keeps the My Mods game files completely separate from the vanilla game.

To create a new My Mods:

1. In the Mod Launcher, open `File > Add New My Mods`.
1. Enter a name of your choice into the "My Server Name" field.
   This will end up being a folder name on your system.
1. Click "Save". This will save the configuration for the new My Mod, but it won't be installed.
1. Navigate to the My Mod you created. It should be under "My Mods" in the navigation pane on the left.
1. Install the new My Mod by clicking "Install Game Copy".
   It is recommended to use the "Copy from an existing copy" option.
   (Note: this will take a while, be patient.)
1. Follow the "Managing Modlets" instructions, above, to install whichever modlets you desire.
   Filter by author "khzmusik" to see only my modlets.
1. Run the game by clicking the "Play Mod" button in the Mod Launcher.

### Installation into the "vanilla" 7D2D game folder

> As of Alpha 20, this will also work for modlets with custom code in a .dll file.

With this method, you will download the source code from the GitLab site,
and move the code directly into your 7D2D game folder.

1. Open the GitLab page for this project:
   
   [https://gitlab.com/karlgiesing/7d2d-a19-modlets](https://gitlab.com/karlgiesing/7d2d-a19-modlets)
1. Make sure you are on the master branch (by default, you should be).
1. On the middle right-hand side of the page, below the description, will be a download button;
   click it and select the compression type of your choice (e.g. "zip" for Windows users).
1. Download the compressed file to a directory of your choice.
1. Uncompress the file. This will create a folder called `7d2d-a19-modlets-master`.
1. Open up your 7D2D game folder. On Windows, it is usually located here:
   
   `C:\Program Files (x86)\Steam\steamapps\common\7 Days To Die`
1. Create a `Mods` folder if one does not already exist.
1. Move any desired modlets from the `7d2d-modlets-master` directory into the game's `Mods` folder.

The next time the game is started, these modlets will be active.

### Installation into the Mod Launcher's MyMods game folder

You should only need to do this if the modlet is not (yet) displayed in the mod launcher.
This only applies to *new* modlets - updates to existing modlets should be handled automatically.
New modlets should automatically show up, but there is often a significant delay before they do.

1. Create a new "My Mods" as detailed above, but stop at the "Manage Modlets" step.
   This will create an entirely new game folder (by default, in `C:\7D2D\AlphaXX\My_Mods`).
1. Download the source code from the GitLab site, and install it as if you were installing it
   in the vanilla game.
   But instead of using the vanilla 7D2D game folder, use the "My Mods" game folder from the
   previous step.

Once the Mod Launcher "catches up" and recognizes the new modlet, you should be able to install
updates automatically, as if you installed them from the Mod Launcher in the first place.

### Installation using DMT

> **Recommended only for advanced users.**
> 
> The DMT tool is designed for modders who are writing their own modlets with custom C# code.
> Though it certainly can be used as a modlet manager,
> it is probably too complicated for casual users.

With this method, you will download the source code from the GitLab site,
and compile the modlet(s) using the DMT tool.

The DMT tool can be found here:

> [DMT tool in the 7D2D forums](https://community.7daystodie.com/topic/13037-dmt-modding-tool/)
> 
> [GitHub repository](https://github.com/HAL-NINE-THOUSAND/DMT)

DMT's Harmony documentation, including DMT installation and configuration instructions, is here:

> [Harmony Docs](https://7d2dmods.github.io/HarmonyDocs/)

Once you have DMT installed and configured, you can use it to install any modlets, including those requiring DMT.

1. Open the GitLab page for this project:
   
   [https://gitlab.com/karlgiesing/7d2d-a19-modlets](https://gitlab.com/karlgiesing/7d2d-a19-modlets)
1. Make sure you are on the master branch (by default, you should be).
1. On the middle right-hand side of the page, below the description, will be a download button;
   click it and select the compression type of your choice (e.g. "zip" for Windows users).
1. Download the compressed file to a directory of your choice.
1. Uncompress the file. This will create a folder called `7d2d-a19-modlets-master`.
1. Move any desired modlets from the `7d2d-modlets-master` directory into a directory of your choice.
   If you are using any other DMT modlets, put them in the same folder as the other modlets.
1. Follow the
   [Harmony Docs instructions](https://7d2dmods.github.io/HarmonyDocs/index.htm?InitialSteps.html)
   to build the modlet(s) and patch them into the game.

The next time the game is started, these modlets will be active.

## Support
The easiest way to contact me is
[through the 7D2D forums](https://community.7daystodie.com/profile/51265-khzmusik/).

If desired, you can also raise an issue in GitLab.

New feature requests are welcome,
but there is no guarantee that I am able (or willing) to do them.

## Contributing
I would greatly appreciate any help with translations.
I have included translations for the texts in most modlets,
but they were generated through an online translation tool,
and probably sound ridiculous to native speakers.

Additionally, any help with testing or balancing is welcome.
I do not play multiplayer games, so reporting any multiplayer bugs is especially welcome.

I may also need help with specific modlets.
If so, I will ask for help in the README.md file of those modlets.

## License
[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)

I hereby dedicate all of these modlets to the public domain.

> **This license applies only to my own original work.**
> 
> It does _not_ apply to any text, graphics, code,
> or other works created by other people or organizations,
> including any works that I modify in these modlets.
> This applies to any works by The Fun Pimps, other mod/modlet authors,
> or creators of purchased Unity assets.

If I have incorporated or modified the works of any other authors,
they should be credited in the `README.md` file for the modlet which uses those works.
If this is not the case, please let me know, and I will fix this as soon as possible.

