# 7D2D Development Notes

## Function keys

* `F1`: open command console
* `F2`:
* `F3`: press f3, then use the button called ply on the menu that appears and on the right you can search and see Cvars and active buffs.
  * Note: Holding `shift` will show you the cvars and buffs of the entity you're looking at.
* `F4`:
* `F5`: toggle third person
* `F6`: toggle entity spawner
* `F7`: toggle HUD
* `F8`: cycle information (framerate, etc)
* `F9`: take screenshot -
  EDIT: as of A20.5 they will be in `%appdata%\7DaysToDie\Screenshots`
* `F10`:
* `F11`:
* `F12`:

## Useful Shortcut keys
(debug mode)

* `0` (number pad): toggle entity (zombie/NPC) info
* `*` (number pad): toggle AI
* `p`: toggle camera lock
* `ctrl-right-click`: teleport (in map)
* `alt`: toggle first/third person camera

## Useful Console commands
https://7daystodie.gamepedia.com/Command_Console

* `cm`: toggle creative menu
* `dm`: toggle debug mode
* `exportcurrentconfigs`: export (possibly modified) configuration XML to
  `Saves/<worldname>/<savegamename>/Configs`
* `gfx af <0 or 1>`: disable/enable anisotropic filtering
* `buff <buff name>`: give buff; if name not given, lists available buffs
* `debuff <buff name>`: remove buff; if name not given, lists active buffs on player

## Prefab Editing

### POI editor

* `<shift>-<alt>-<right click>`: when looking at a trader block, selects the trader
    * Make sure you hit the (nearly invisible) "OK" button in the lower left
* `<shift>-<enter>`: open POI Marker Editor

### Updating POIs in A20

1. Load up the game and go into the prefab editor.
2. Press F1 to open the console.
3. Run this command: `prefabupdater loadtable`
4. Then run this command: `prefabupdater updateblocks`

### Prefab Sizes
(and the tiles that hold them)

These are the only "oldwest" tiles that spawn anything other than Extra Small and Custom 25x50:
* `rwg_tile_oldwest_corner`
* `rwg_tile_oldwest_t`

* Extra Small: 25x25
  * `rwg_tile_countryresidential_cap`
  * `rwg_tile_countryresidential_corner`
  * `rwg_tile_countryresidential_intersection`
  * `rwg_tile_countryresidential_straight`
  * `rwg_tile_countryresidential_t`
  * `rwg_tile_countrytown_cap`
  * `rwg_tile_countrytown_corner`
  * `rwg_tile_countrytown_intersection`
  * `rwg_tile_countrytown_straight`
  * `rwg_tile_countrytown_t`
  * `rwg_tile_downtown_cap`
  * `rwg_tile_downtown_corner`
  * `rwg_tile_downtown_intersection`
  * `rwg_tile_downtown_straight`
  * `rwg_tile_downtown_t`
  * `rwg_tile_industrial_cap`
  * `rwg_tile_industrial_corner`
  * `rwg_tile_industrial_straight`
  * `rwg_tile_oldwest_cap`
  * `rwg_tile_oldwest_corner`
  * `rwg_tile_oldwest_intersection`
  * `rwg_tile_oldwest_straight`
  * `rwg_tile_oldwest_t`
* Small: 42x42
  * `rwg_tile_commercial_cap`
  * `rwg_tile_commercial_corner`
  * `rwg_tile_commercial_intersection`
  * `rwg_tile_commercial_straight`
  * `rwg_tile_commercial_t`
  * `rwg_tile_countryresidential_cap`
  * `rwg_tile_countryresidential_corner`
  * `rwg_tile_countryresidential_intersection`
  * `rwg_tile_countryresidential_straight`
  * `rwg_tile_countryresidential_t`
  * `rwg_tile_countrytown_cap`
  * `rwg_tile_countrytown_corner`
  * `rwg_tile_countrytown_intersection`
  * `rwg_tile_countrytown_straight`
  * `rwg_tile_countrytown_t`
  * `rwg_tile_downtown_cap`
  * `rwg_tile_downtown_corner`
  * `rwg_tile_downtown_intersection_02`
  * `rwg_tile_downtown_intersection`
  * `rwg_tile_downtown_straight`
  * `rwg_tile_downtown_t`
  * `rwg_tile_industrial_corner`
  * `rwg_tile_industrial_intersection`
  * `rwg_tile_industrial_straight`
  * `rwg_tile_industrial_t`
  * `rwg_tile_oldwest_corner`
  * `rwg_tile_oldwest_t`
  * `rwg_tile_residential_cap`
  * `rwg_tile_residential_corner`
  * `rwg_tile_residential_intersection`
  * `rwg_tile_residential_straight`
  * `rwg_tile_residential_t`
  * `rwg_tile_rural_t_02`
* Medium: 60x60 (this is the largest size POI that will spawn in a vanilla downtown tile.)
  * `rwg_tile_commercial_corner` ("shopping center" sign in road)
  * `rwg_tile_commercial_intersection`
  * `rwg_tile_commercial_straight`
  * `rwg_tile_commercial_t`
  * `rwg_tile_countrytown_corner`
  * `rwg_tile_countrytown_intersection`
  * `rwg_tile_countrytown_t`
  * `rwg_tile_downtown_intersection_02` (overpass)
  * `rwg_tile_downtown_t`
  * `rwg_tile_industrial_intersection` (underpass)
  * `rwg_tile_industrial_t` (canal)
  * `rwg_tile_rural_corner`
  * `rwg_tile_rural_intersection` (w/ambulance, usually next to trader)
  * `rwg_tile_rural_t`
  * `rwg_tile_rural_t_02`
  * `rwg_tile_rural_t_03`
* Large: 100x100
  * `rwg_tile_commercial_cap`
  * `rwg_tile_rural_cap`
  * `rwg_tile_rural_straight`
* Custom: 25x50
  * All "oldwest" tiles

### Prefab Rules
For human NPCs, from Stallionsden/Compo Pack.

#### Tiers

* T1: 1 to 10 NPCs  (low level NPCs only)
* T2: 11 to 20 NPCs (low level NPCs only)
* T3: 21 to 35 NPCs (medium level NPCs only)
* T4: 36 to 50 NPCs (medium level NPCs only)
* T5: 51 to 100 NPCs (High level NPCs only)

#### Loot
This is for _zombie_ POIs; humans use the same but this may be updated in A21.

* T1:
    
    1-5 Zombies: 
  * Use the Part Name: part_loot_t1 Part Spawn Chance at max 0.2 
    
  6-15 Zombies: 
  * 1 x T1 Wood Crate 
  * 1 x (Small) Weapons Bag or 1 x Gun Safe
  * 1 x Small Food Pile Random Helper
  * 1 x Ammo Pile, Small 
  * 1 x (Small) Medical Supplies or Small Chemistry Set
  * 1 x Random Crate (unless a themed prefab.) 
* T2:
  * Use the Part Name: part_loot_t2 Part Spawn Chance at 1.0

    **OR**
  * 1 x T2 Leather Trunk
  * 1 x Desk/Wall Safe
  * 1 x (Small) Weapons Bag 
  * 1 x Food Pile, Medium
  * 1 x Ammo Pile, Medium
  * 2 x (Small) Medical Supplies or Small Chemistry Sets
  * 2 x Store/Random Crate 
* T3:
  * Use the Part Name: part_loot_t3 Part Spawn Chance at 1.0

    **OR**
  * 1 x T3 Reinforced Chest (Locked)
  * 3 x Store/Random Crates (Pending size of POI)
  * 1 x Gun Safe or Munition Crate
  * 1 x (Large) Weapons Bag
  * 1 x (Small) Weapons Bag
  * 1 x Food Pile, Medium
  * 1 x Ammo Pile, Medium
  * 1 x (Small) Medical Supplies or Small Chemistry Set
* T4:
  * Use the Part Name: part_loot_t4 Part Spawn Chance at 1.0

    **OR**
  * 1 x T4 Hardened Chest (Single-block, Locked)
  * 4 x Store/Random Crates (Pending size of POI)
  * 1 x Gun Safe or Munition Crate
  * 1 x (Large) Weapons Bag
  * 1 x (Small) Weapons Bag
  * 1 x Food Pile, Medium
  * 1 x Ammo Pile, Medium
  * 1 x (Medium) Medical Supplies/Cabinet or Medium Chemistry Set
* T5:
  * Use the Part Name: part_loot_t5 Part Spawn Chance at 1.0

    **OR**
  * 1 x T5 Hardened Chest (Double-block, Locked)
  * 3 of the following: Gun Safe, Munition Crate, Hidden Crate (should be hidden), or (Large) Weapons Bag.
  * 1 x (Small) Weapons Bag
 
    _Variable Loot_
  * 5 x Store/Random Crates | +3 for every additional 100 zombies after 100
  * 1 x Food Pile, Large | +1 for every additional 75 zombies after 75
  * 1 x Ammo Pile, Large | +1 for every additional 75 zombies after 75
  * 1 x (Large) Medical Supplies/Cabinet or Large Chemistry Set | +1 for every additional 75 zombies after 75

## Visual Studio

To set up a project to launch from a non-Steam game directory (e.g. Mod Launcher installed):

* Project (menu item) -> [project name] Properties
* Debug (tab on left)
* Configuration (drop-down at top): `All Configurations`
* Start external program (select): `C:\7D2D\Custom\MyMods\[MyMod name]\7DaysToDie.exe`
* Command line arguments: `-logfile "C:\7D2D\Custom\MyMods\[MyMod name]\7DaysToDie_Data\output_log.txt" "-savegamefolder=C:\7D2D\Saves\MyMods\[MyMod name]" -nogs -noeac`
* Working directory: `C:\7D2D\Custom\MyMods\[MyMod name]\`
