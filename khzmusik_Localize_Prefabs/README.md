# Localize Prefabs

This mod localizes the names of prefabs when they appear in quest dialogs.

## Changes and Features

* Uses the `Localization.txt` file to display the localized names of prefabs, instead of filenames.
* Includes a `Localization.txt` file that provides names for all vanilla prefabs that can be
    quest destinations.
* Will automatically pick up any other localization files, so prefab authors can include their own
    `Localization.txt` file, and their prefabs should be translated by this mod.

## Technical Details

**This modlet contains custom C# code.**
It is *not* compatible with EAC.
It must be installed on both clients and servers.

This mod does not modify any config files, save game data, or the game in memory.
So, _in theory,_ it should be OK to install or remove this mod to an ongoing game.

However, _any_ mod with custom C# code has a small chance to corrupt game data.
For that reason, I still recommend starting a new game.

## Possible improvements

The non-English prefab names were generated using automated translation software.
There are almost certainly names that are translated incorrectly.

If there are non-English speakers who are willing to tranlate the names by hand, please contact me.
I need all the help I can get!
